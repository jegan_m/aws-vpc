#variables for VPC creation

variable "projectname"{
default=""
}

variable "environment"{
default=""
}

variable "vpc_cidr"{
default="10.0.0.0/16"
}

variable "enable_dns_support"{
default=true
}

variable "enable_dns_hostnames"{
default=true
}

#variables and data source for subnet creation

data "aws_availability_zones" "az"{
state = "available"
}

# variables and data source for NAT instance creation

data "aws_ami" "nat"{
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-vpc-nat-hvm-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners=["137112412989"]
}

variable "instance_initiated_shutdown_behavior"{
    default="stop"
}

variable "nat-ins-type"{
    default="t2.small"
}

variable "key-pair"{
    default=""
}

variable "disable_api_termination" {
    default = ""  
}

# Output purpose

/*data "aws_subnet_ids" "public-subnet-ids"{
  vpc_id="${aws_vpc.main.id}"
  filter{
    name   = "tag:Name"
    values=["${var.projectname}-${var.environment}-public subnet-*"]
  }
}

data "aws_subnet_ids" "private-subnet-ids"{
  vpc_id="${aws_vpc.main.id}"
  filter{
    name   = "tag:Name"
    values=["${var.projectname}-${var.environment}-private subnet-*"]
  }
}*/
