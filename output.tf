output "az"{
    value="${data.aws_availability_zones.az.names}"
    description = "To know how much az is available in this region"
}

output "private-subnet-ids"{
    value="${aws_subnet.private-subnet.*.id}"
    description = "To know subnet id for launch instance reside under corresponding subnet"
}

output "public-subnet-ids"{
    value=aws_subnet.public-subnet[*].id
    description = "To know subnet id for launch instance reside under corresponding subnet"
}

output "vpc_id"{
    value="${aws_vpc.main.id}"
    description = "To get VPC id"
}

output "nat-nic-id"{
    value="${aws_instance.nat.primary_network_interface_id}"
}

output "public-routetable-id"{
    value = "${aws_route_table.public-subnet.id}"
}

output "private-routetable-id"{
    value = "${aws_vpc.main.main_route_table_id}"
}

output "nat-public-ip"{
    value = "${aws_eip.nat.public_ip}"
}
