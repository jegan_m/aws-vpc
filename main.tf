# VPC creation

resource "aws_vpc" "main"{
cidr_block = "${var.vpc_cidr}"
instance_tenancy="default"
enable_dns_support="${var.enable_dns_support}"
enable_dns_hostnames="${var.enable_dns_hostnames}"
tags={
Name="${var.projectname}-${var.environment}"
}
}

# Subnets creation 

resource "aws_subnet" "public-subnet"{
vpc_id="${aws_vpc.main.id}"
count="${length(data.aws_availability_zones.az.names)}"
availability_zone="${data.aws_availability_zones.az.names[count.index]}"
cidr_block="${cidrsubnet("${aws_vpc.main.cidr_block}", 8, "${count.index}")}" /*cidrsubnet(prefix, newbits, netnum)newbits change network portion in given cidr prefix netnum value used in extended network portion bits only(newbits)*/
map_public_ip_on_launch=true
tags={
Name= "${var.projectname}-${var.environment}-public subnet-${count.index+1}"
}
}

resource "aws_subnet" "private-subnet"{
vpc_id="${aws_vpc.main.id}"
count="${length(data.aws_availability_zones.az.names)}"
availability_zone="${data.aws_availability_zones.az.names[count.index]}"
cidr_block="${cidrsubnet("${aws_vpc.main.cidr_block}", 8, "${count.index}"+"${length(data.aws_availability_zones.az.names)}")}" /*cidrsubnet(prefix, newbits, netnum)newbits change network portion in given cidr prefix netnum value used in extended network portion bits only(newbits)*/
map_public_ip_on_launch=false
tags={
Name= "${var.projectname}-${var.environment}-private subnet-${count.index+1}"
}
}

# Internet Gateway creation

resource "aws_internet_gateway" "igw"{
    vpc_id="${aws_vpc.main.id}"
tags={
    Name="${var.projectname}-${var.environment}-IGW"
}
}

# Route tables creation

resource "aws_route_table" "public-subnet"{
vpc_id="${aws_vpc.main.id}"
route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
}
tags={
    Name="${var.projectname}-${var.environment}-public subnet"
}
}

# Route table association

resource "aws_route_table_association" "public-subnet"{
    count="${length(data.aws_availability_zones.az.names)}" 
    subnet_id="${element(aws_subnet.public-subnet.*.id, count.index)}"
    route_table_id = "${aws_route_table.public-subnet.id}"
}

resource "aws_route_table_association" "private-subnet"{
    count="${length(data.aws_availability_zones.az.names)}" 
    subnet_id="${element(aws_subnet.private-subnet.*.id, count.index)}"
    route_table_id = "${aws_vpc.main.main_route_table_id}"
}


# Add routes in Route table

resource "aws_route" "private-subnet"{
    route_table_id="${aws_vpc.main.main_route_table_id}"
    destination_cidr_block="0.0.0.0/0"
    network_interface_id="${aws_instance.nat.primary_network_interface_id}"
}

# Security Group creation

resource "aws_security_group" "nat-sg" {
  name="Primary Security Group of NAT Server"
  description="Allow inbound and outbound traffic"
  vpc_id="${aws_vpc.main.id}"
  ingress{
      from_port=22
      to_port=22
      protocol="tcp"
      cidr_blocks=["123.201.137.178/32","61.12.45.102/32"]
  }
  egress{
      from_port=0
      to_port=0
      protocol="-1"
      cidr_blocks=["0.0.0.0/0"]
  }
  tags={
      Name="NAT-server-SG"
  }
}

# EIP creation

resource "aws_eip" "nat" {
  instance = "${aws_instance.nat.id}"
  vpc      = true
}

# NAT instance creation

resource "aws_instance" "nat"{
    ami="${data.aws_ami.nat.id}"
    disable_api_termination= "${var.disable_api_termination}"
    instance_initiated_shutdown_behavior="${var.instance_initiated_shutdown_behavior}"
    instance_type="${var.nat-ins-type}"
    key_name="${var.key-pair}"
    vpc_security_group_ids=["${aws_security_group.nat-sg.id}"]
    subnet_id="${element(aws_subnet.public-subnet.*.id, 0)}"
    source_dest_check="false"
    tags={
        Name="${var.projectname}-${var.environment}-NAT-Server"
    }
    root_block_device{
        volume_type="gp2"
        volume_size="10"
        delete_on_termination="true"    
    }

}
